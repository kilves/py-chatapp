import os
import logging
import string

from aiohttp import web
from chatapp.connection import ChatConnection
import aiohttp_jinja2
import jinja2
from coilmq.engine import StompEngine
from coilmq.store import QueueStore
from coilmq.queue import QueueManager
from chatapp.protocol import STOMP11
from chatapp.chat import ChatManager
from chatapp.frame import Frame
from chatapp.chat_queue import ChatQueueManager
from chatapp.auth import ChatAuthenticator

@aiohttp_jinja2.template("index.jinja2")
async def index(request):
    return {"greeting": "world"}

def random_string(length):
    choice = string.ascii_lowercase + string.digits
    randoms = os.urandom(length)
    return "".join(choice[randoms[x] % len(choice)] for x in range(length))

async def register(request):
    data = await request.post()
    username = data["username"]
    password = data["password"]
    password_again = data["password_again"]
    if password != password_again:
        return web.json_response({success: False, message: "Passwords must match"})
    try:
        authenticator.register(username, password)
    except Exception:
        return web.json_response({success: False, message: "Unknown error"})
    return web.json_response({success: True})

async def first_register(request):
    username = random_string(16)
    password = random_string(16)
    try:
        authenticator.register(username, password, simple=True)
    except Exception:
        return web.json_response({success: False, message: "Unknown error."})
    return web.json_response({"success": True, "username": username, "password": password})

async def ws_handler(request):
    ws = web.WebSocketResponse(protocols=["v11.stomp"], heartbeat=1)

    connection = ChatConnection(ws)
    queue_manager = ChatQueueManager(QueueStore())
    engine = StompEngine(connection,
                         authenticator,
                         queue_manager,
                         chat_manager,
                         protocol=STOMP11)

    await ws.prepare(request)
    async for msg in ws:
        if msg.type == web.MsgType.text:
            frame = Frame()
            frame.sender = connection
            try:
                frame.unpack(msg.data)
                engine.process_frame(frame)
            except ValueError as e:
                logger.error(e)
        elif msg.type == web.MsgType.binary:
            await ws.send_bytes(msg.data)
        elif msg.type == web.MsgType.close:
            await ws.close()
            break
    engine.unbind()
    return ws

authenticator = ChatAuthenticator()
chat_manager = ChatManager()
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("chatapp")
app = web.Application()
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader("./templates"))
app.router.add_get("/", index)
app.router.add_get("/ws", ws_handler)
app.router.add_post("/register", register)
app.router.add_post("/first_register", first_register)
app.router.add_static("/res", "./res")
web.run_app(app)
