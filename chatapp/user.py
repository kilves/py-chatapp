import chatapp.database as database

class User(object):

    def __init__(self, username, password):
        self.cursor = database.connection.cursor()
        self.name = username,
        self.password = password
        self.display_name = None

    @classmethod
    def from_db(cls, db_object):
        user = User(db_object["name"], db_object["password"])
        user.id = db_object["id"]
        user.simple =db_object["simple"]
        user.display_name = db_object["display_name"]
        return user

    @staticmethod
    def load(username):
        cursor = database.connection.cursor()
        query = "SELECT * FROM users WHERE name=%s"
        cursor.execute(query, (username,))
        db_object = cursor.fetchone()
        if db_object:
            u = User.from_db(db_object)
            return u

    def save(self):
        query = "INSERT INTO users(name, display_name, password) " \
                "VALUES(%s, %s, %s)" \
                "ON CONFLICT (name)" \
                "DO UPDATE SET name=%s, display_name=%s, password=%s"
        self.cursor.execute(query, (self.name, self.password, self.display_name, self.id))
        database.connection.commit()