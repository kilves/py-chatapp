import os

from coilmq.auth import Authenticator
from passlib.hash import argon2
import chatapp.database as database
from chatapp.user import  User


class ChatAuthenticator(Authenticator):

    @staticmethod
    def hash_password(password):
        return argon2.hash(password)

    @staticmethod
    def verify_password(hashed_password, guessed_password):
        return argon2.verify(guessed_password, hashed_password)

    def authenticate(self, login, password):
        """
        Authenticates the user
        :param login: the username
        :param password: password
        :return: The user object of this session
        """
        user = User.load(login)
        if self.verify_password(user.password, password):
            return user
        return

    def register(self, username, password, simple=False):
        password_hash = self.hash_password(password)
        cursor = database.connection.cursor()
        query = "INSERT INTO users(name, password, simple) VALUES(%s, %s, %s);"
        cursor.execute(query, (username, password_hash, simple))
        database.connection.commit()

