import json
import chatapp

class ChatEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, chatapp.chat.ChatChannel):
            return {
                "name": o.name,
                "type": str(o.type)
            }
        return {'__{}__'.format(o.__class__.__name__): o.__dict__}
