from collections import defaultdict
import threading
import uuid
from enum import Enum

from coilmq.topic import TopicManager
from coilmq.util.concurrency import synchronized
from chatapp.frame import MessageFrame, ErrorFrame
from chatapp.util.json import ChatEncoder
import json

lock = threading.RLock()


class ChatChannel(object):
    class Type(Enum):
        ANON = 1
        NOT_ANON = 2

        def __str__(self):
            if self.value == 1:
                return "anon"
            elif self.value == 2:
                return "not_anon"

    def __init__(self, name, typ=Type.ANON):
        self.type = typ
        self.name = name

    def is_anon(self):
        return self.type == self.Type.ANON


class ChatManager(TopicManager):
    def __init__(self):
        self.chat_channels = {
            "anonyymit": ChatChannel("anonyymit"),
            "ei-anonyymi": ChatChannel("ei-anonyymi", ChatChannel.Type.NOT_ANON)
        }
        super().__init__()
        self._destinations = defaultdict(set)
        self._subscriptions = defaultdict(set)
        self._connections = dict()

    def get_channel(self, destination):
        return self.chat_channels[destination.split("/")[2]]

    def transmit_channels(self, connection, subscription_id):
        msg_body = json.dumps(list(self.chat_channels.values()), cls=ChatEncoder)
        msg = MessageFrame(destination="/channels",
                           body=msg_body,
                           subscription_id=subscription_id)
        connection.send_frame(msg)

    def close(self):
        super().close()

    def add_connection(self, connection):
        self._connections[connection.uuid] = connection

    def is_valid_channel(self, destination):
        if destination.startswith("/channel/"):
            channel_name = destination.split("/")[2]
            if channel_name in self.chat_channels:
                return True
        return False

    def get_subscription_errors(self, connection, destination):
        if not self.is_valid_channel(destination):
            return "Channel doesn't exist."
        if connection.user.simple and not self.get_channel(destination).is_anon():
            return "This is a non-anonymous channel, and you need an account to join."
        return False

    def send_error(self, connection, message, body, subscription_id=None, destination=None):
        extra_headers = {}
        if subscription_id:
            extra_headers["subscription"] = subscription_id
        if destination:
            extra_headers["destination"] = destination
        connection.send_frame(ErrorFrame(message, body=body, extra_headers=extra_headers))

    @synchronized(lock)
    def subscribe(self, connection, destination, subscription_id):
        if destination == "/channels":
            self.transmit_channels(connection, subscription_id)
            return
        possible_error = self.get_subscription_errors(connection, destination)
        if possible_error:
            self.send_error(connection, "Unable to join channel", possible_error, subscription_id, destination)
            return
        self.log.debug("Subscribing %s to %s" % (connection, destination))
        self._topics[destination].add(connection.uuid)
        self._subscriptions[connection.uuid].add(subscription_id)
        self._destinations[subscription_id] = destination

    @synchronized(lock)
    def unsubscribe(self, connection, subscription_id):
        if subscription_id not in self._destinations:
            raise ValueError("Invalid subscription id")
        destination = self._destinations[subscription_id]
        self.log.debug("Unsubscribing %s from %s" % (connection, destination))
        self._topics[destination].remove(connection.uuid)
        self._subscriptions[connection.uuid].remove(subscription_id)
        if not self._topics[destination]:
            del self._topics[destination]
        del self._destinations[subscription_id]

    @synchronized(lock)
    def disconnect(self, connection):
        self.log.debug("Disconnecting %s" % connection)
        del self._connections[connection.uuid]
        for sub_id in self._subscriptions[connection.uuid]:
            self._topics[self._destinations[sub_id]].remove(connection.uuid)
            del self._destinations[sub_id]
        del self._subscriptions[connection.uuid]

    @synchronized(lock)
    def send(self, message):
        dest = message.headers.get('destination')
        if not dest:
            raise ValueError(
                "Cannot send frame with no destination: %s" % message)

        message.cmd = 'MESSAGE'
        message_json = json.loads(message.body)
        message.body = json.dumps({
            "text": message_json["text"][:500],
            "type": "text",
            "from": message.sender.uuid.hex[:5]
        })
        message.headers.setdefault('message-id', str(uuid.uuid4()))
        bad_subscribers = set()
        for connection_uuid in self._topics[dest]:
            connection = self._connections[connection_uuid]
            for subscription_id in self._subscriptions[connection_uuid]:
                if dest == self._destinations[subscription_id]:
                    message.headers['subscription'] = subscription_id
                    break
            try:
                connection.send_frame(message)
            except:
                self.log.exception(
                    "Error delivering message to subscriber %s; client will be disconnected." % subscriber)
                # We queue for deletion so we are not modifying the topics dict
                # while iterating over it.
                bad_subscribers.add(connection)

        for subscriber in bad_subscribers:
            self.disconnect(subscriber)
