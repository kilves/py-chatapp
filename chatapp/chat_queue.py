import threading

from coilmq.queue import QueueManager
from coilmq.util.concurrency import synchronized

lock = threading.RLock()

class ChatQueueManager(QueueManager):
    @synchronized(lock)
    def subscribe(self, connection, destination, message_id):
        self.log.debug("Subscribing %s to %s" % (connection, destination))
        self._queues[destination].add((connection, message_id))
        self._send_backlog(connection, destination)
        pass

    @synchronized(lock)
    def unsubscribe(self, connection, destination, message_id):
        self.log.debug("Unsubscribing %s from %s" % (connection, destination))
        queue = (connection, message_id)
        if queue in self._queues[destination]:
            self._queues[destination].remove(queue)

        if not self._queues[destination]:
            del self._queues[destination]


    @synchronized(lock)
    def send(self, message):

        dest = message.headers.get('destination')
        if not dest:
            raise ValueError(
                "Cannot send frame with no destination: %s" % message)

        message.cmd = 'MESSAGE'

        message.headers.setdefault('message-id', str(uuid.uuid4()))
        message.headers["subscription"]

        # Grab all subscribers for this destination that do not have pending
        # frames
        subscribers = [s[0] for s in self._queues[dest]
                       if s[0] not in self._pending]

        if not subscribers:
            self.log.debug(
                "No eligible subscribers; adding message %s to queue %s" % (message, dest))
            self.store.enqueue(dest, message)
        else:
            selected = self.subscriber_scheduler.choice(subscribers, message)
            message.headers[""]
            self.log.debug("Delivering message %s to subscriber %s" %
                           (message, selected))
            self._send_frame(selected, message)