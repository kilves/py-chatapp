import uuid

class ChatConnection():
    def __init__(self, ws):
        self.ws = ws
        self.uuid = uuid.uuid4()
        pass

    def send_frame(self, frame):
        try:
            self.ws.send_bytes(frame.pack())
        except TypeError as e:
            pass
