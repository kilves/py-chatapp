import json
import psycopg2cffi as psy
from psycopg2cffi.extras import DictCursor
connection = None

with open("db.config") as f:
    settings = json.loads(f.read())
    print("CONNECT DB")
    connection = psy.connect(cursor_factory=DictCursor, **settings)