from distutils.core import setup

setup(
    name="chatapp",
    version="0.1.0",
    author="Petri Hetta",
    packages=["chatapp"],
    description="A chat application for web",
    install_requires=[
        "aiohttp >= 2.3.7",
        "aiohttp_jinja2 => 0.14.0",
        "coilmq => 1.0.1",
        "psycopg2cffi => 2.7.7",
        "passlib => 1.7.1",
        "argon2_cffi => 18.1.0"
    ]
)