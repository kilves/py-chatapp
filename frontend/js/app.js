module.exports.app = angular.module("hello", []);

module.exports.app.config(["$interpolateProvider",
	function ($interpolateProvider) {
		$interpolateProvider.startSymbol("[[");
		$interpolateProvider.endSymbol("]]");
	}
]);
