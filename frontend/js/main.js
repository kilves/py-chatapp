"use strict";
var Cookies = require("js-cookie");
var account = require("./account.js");
var app = require("./app.js").app;
var chatService = require("./chat.js");

app.controller("chat", ["$scope", "chatService", function ($scope, chatService) {
	$scope.activeChannel = "";

	$scope.chatMessages = function () {
		if ($scope.activeChannel === "")
			return [];
		return chatService.channels[$scope.activeChannel].messages;
	}
	$scope.channels = function () {
		return chatService.channels;
	}
	$scope.subscribedChannels = function () {
		return Object.values(chatService.subscribedChannels);
	}
	chatService.applyScope = function () {
		$scope.$apply();
	}
	$scope.messageToSend = {
		text: ""
	};

	$scope.keysend = function (e) {
		e.preventDefault();
		var charCode = (e.which) ? e.which : e.keyCode;
		if (charCode === 13) {
			$scope.send();
		}
	}

	$scope.send = function () {
		chatService.sendChatMessage($scope.activeChannel, $scope.messageToSend);
		var textarea = document.getElementById("newmessage-textarea");
		angular.element(textarea).val("");
	};

	$scope.subscribeToChannel = function ($event, channelName) {
		$event.preventDefault();
		chatService.subscribeTo(channelName);
		$scope.activeChannel = channelName;
	}

	$scope.showChannel = function ($event, channelName) {
		$event.preventDefault();
		$scope.activeChannel = channelName;
	}

	$scope.isActiveChannel = function (channelName) {
		return channelName === $scope.activeChannel;
	}
}]);
