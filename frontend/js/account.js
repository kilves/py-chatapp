var $ = require("jquery");
var Cookies = require("js-cookie");

module.exports.createSimple = function (callback) {
	$.post("first_register", function (res) {
		if (!res.success) {
			callback(null, res.message);
			return;
		}
		Cookies.set("username", res.username, {
			expires: 365
		});
		Cookies.set("password", res.password, {
			expires: 365
		});
		callback({
			username: res.username,
			password: res.password
		});
	});
}

module.exports.getCredentials = function () {
	return {
		username: Cookies.get("username"),
		password: Cookies.get("password")
	};
}

module.exports.getType = function () {
	var username = Cookies.get("username");
	var password = Cookies.get("password");
	var token = Cookies.get("token");
	if ((username && password))
		return "simple";
	if (token)
		return "full";
	return "none";
}
