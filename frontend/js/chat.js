var app = require("./app.js").app;
var Stomp = require("webstomp-client");
var account = require("./account.js");

app.service("chatService", function () {
	var stomp = Stomp.client("ws://localhost:8080/ws", {
		"heartbeat": false
	});
	this.channels = {};
	this.subscribedChannels = {};
	this.applyScope = function () {}

	this.sendChatMessage = function (channelName, chatMessage) {
		console.log(channelName);
		stomp.send("/channel/" + channelName, JSON.stringify(chatMessage));
	}

	this.onChatMessage = function (message) {
		console.log(message);
	}

	this.parseChannelName = function (destination) {
		if (!destination)
			return;
		var destsplit = destination.split("/");
		if (destsplit.length === 3)
			return destsplit[2];
		return "";
	}

	this.printError = function (channelName, message) {
		if (!this.channels.hasOwnProperty(channelName))
			return;
		this.channels[channelName].messages.push({
			type: "error",
			text: message
		});
		this.applyScope();
	}

	this.subscribeTo = function (channelName) {
		if (!this.channels.hasOwnProperty(channelName))
			return;
		if (this.channels[channelName].subscribed)
			return;
		this.subscribedChannels[channelName] = this.channels[channelName];
		this.channels[channelName].messages = [];
		this.channels[channelName].subscribed = true;
		stomp.subscribe("/channel/" + channelName, (message) => {
			var message = JSON.parse(message.body);
			this.channels[channelName].messages.push(message);
			this.applyScope();
		});
	}

	this.initialize = function () {
		var accountType = account.getType();
		if (accountType == "none") {
			account.createSimple(function (response, err) {
				if (!response) {
					alert(err);
					return;
				}
				print("OK")
				this.stompConnect(response.username, response.password);
			});
		} else if (accountType == "simple" || accountType == "full") {
			var credentials = account.getCredentials();
			this.stompConnect(credentials.username, credentials.password);
		}
	}

	this.stompConnect = function (username, password) {
		stomp.connect({
			"login": username,
			"passcode": password
		}, (frame) => {
			stomp.subscribe("/channels", (message) => {
				var channels = JSON.parse(message.body);
				for (let channel of channels) {
					this.channels[channel.name] = channel;
				}
				this.applyScope();
			});
		}, (error) => {
			var dest = this.parseChannelName(rror.headers.destination);
			if (dest) {
				this.printError(dest, error.body);
				this.channels[dest].subscribed = false;
			}
		});
	}

	this.initialize();

	function listener(data) {
		switch (data.command) {
		case "addMessage":
			chatMessages.push(data.chatMessage);
		}
	}
})
